@extends('layouts.dashboard')
@section('page-name','Packages')


@section('content')

<?php 

function MBtoGB($data)
    {
        if ($data >= 1024)
        {
            $data = number_format($data / 1024, 2) . ' GB';
        }
        elseif ($data > 1)
        {
            $data = $data . ' MB';
        }
        elseif ($data == 1)
        {
            $data = $data . ' MB';
        }
        else
        {
            $data = 'Unlimited';
        }

        return $data;
}
 ?>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title m-b-0">Packages Management</h5> <br>
          <a class="btn btn-primary" href="{{ route('package.sync') }}"><i class="mdi mdi-refresh font-16"></i> Sync From Server</a>
        </div>
        <div class="table-responsive">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Disk Space</th>
                <th scope="col">Bandwith</th>
                <th scope="col">Domain's</th>
                <th scope="col">Email</th>
                <th scope="col">DB</th>
                <th scope="col">FTP</th>
              </tr>
            </thead>
            <tbody class="customtable">
              @foreach($data['list'] as $item)
              <tr>
                <td><button class="btn btn-sm btn-primary btn-edit-package" data-id="{{ $item->id }}" data-name="{{ $item->title }}" data-price="{{ $item->price }}" data-toggle="modal" data-target="#editModal">{{ $item->title }}</button></td>
                <td>{{ MBtoGB($item->quota) }}</td>
                <td>{{ MBtoGB($item->bandwidth) }}</td>
                <td>{{ $item->domain }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ ucwords($item->db) }}</td>
                <td>{{ ucwords($item->ftp) }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Package</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('package.update') }}" method="post" id="submitForm">
        @csrf
        <input type="hidden" name="id" value="" id="form-id">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="title" class="form-control" id="form-name">
        </div>
        <div class="form-group">
          <label>Price</label>
          <input type="text" name="price" class="form-control" id="form-price">
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-save-package">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('custom-script')
<script>
  $(document).ready(function(){
    $(document).on('click','.btn-edit-package',function(){
      var id = $(this).data('id');
      var name = $(this).data('name');
      var price = $(this).data('price');
      $('#form-id').val(id);
      $('#form-name').val(name);
      $('#form-price').val(price);
    });
    $(document).on('click','.btn-save-package',function(){
      $('#submitForm').submit();
    });
  });
</script>
@endsection
