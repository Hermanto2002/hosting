@extends('layouts.dashboard')
@section('page-name','Hosting Module')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title m-b-0">Create Hosting</h5>
        </div>
        <div class="table-responsive">
          <div class="card">
            <form action="{{ route('module.hosting.build') }}" method="post">
              @csrf
            <div class="card-body">
              <div class="form-group">
                <label>Domain</label>
                <input type="text" class="form-control" name="domain" placeholder="domain.com">
              </div>
              <div class="form-group">
                <label>Email Notification</label>
                <input type="email" class="form-control" name="email">
              </div>
              <div class="form-group">
                <label>Package's</label>
                <select name="package" class="form-control">
                  <option value="STARTER">Starter [150MB/15GB]</option>
                  <option value="Small">Small [250MB/20GB]</option>
                  <option value="Standard">Standard [600MB/55GB]</option>
                  <option value="BIG">Big [1GB/100GB]</option>
                  <option value="SUPER">Super [2GB/200GB]</option>
                </select>
              </div>

              <div class="float-right">
                <button class="btn btn-success">Create Hosting</button>
              </div>

            </div>

            </form>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
@endsection