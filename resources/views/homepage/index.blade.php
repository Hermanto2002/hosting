 @extends('layouts.homepage')
 @section('content')
 <!-- Hero Section Begin -->
 <section class="hero-section">
    <div class="set-bg" data-setbg="{{ asset('homepage/img/hero/hero-1.jpg')}}">
        <div class="container">
            <div class="row" style="padding: 180px 0px 180px 0px;">
                <div class="col-lg-6">
                    <div class="text-white" style="margin-top: 200px;">
                        <h2>HOSTING UNTUK BISNIS ANDA!</h2>
                        <h5 class="mt-3">Hosting dengan kualitas terbaik kaya akan fitur yang akan membuat website anda sangat cepat dan lancar untuk di akses</h5>
                        <a href="#hosting" class="primary-btn mt-4">Get started now</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="hero__img">
                        <img src="{{ asset('homepage/img/hero/hero-right.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->

<!-- Register Domain Section Begin -->
<section class="register-domain spad">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-lg-8">
                <div class="register__text">
                    <div class="section-title">
                        <h3>Register Your Domain Now!</h3>
                    </div>
                    <div class="register__form">
                        <form action="#">
                            <input type="text" placeholder="ex: cloudhost">
                            <button type="submit" class="site-btn">Search</button>
                        </form>
                    </div>
                    <div class="register__result">
                        <ul>
                            <li>.com <span>$1.95</span></li>
                            <li>.net <span>$1.95</span></li>
                            <li>.org <span>$1.95</span></li>
                            <li>.us <span>$1.95</span></li>
                            <li>.in <span>$1.95</span></li>
                        </ul>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Register Domain Section End -->

<!-- Services Section Begin -->
<section id="hosting" class="services-section spad">

    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7">
                <div class="section-title normal-title">
                    <h3>Pilih Paket Web Hosting</h3>
                </div>
            </div>
            <div class="col-lg-3 offset-lg-2 col-md-5">
                <div class="pricing__swipe-btn">
                    <label for="id" class="active" style="width: 50%">ID
                        <input type="ID" id="ID">
                    </label>
                    <label for="usa" style="width: 50%">USA
                        <input type="USA" id="USA">
                    </label>
                </div>
            </div>
        </div>
        <div class="row monthly__plans active">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="pricing__item">
                    <h4>indo</h4>
                    <h3>$15.90 <span>/ month</span></h3>
                    <ul>
                        <li>2,5 GB web space</li>
                        <li>Free site buiding tools</li>
                        <li>Free domain registar</li>
                        <li>24/7 Support</li>
                        <li>Free marketing tool</li>
                        <li>99,9% Services uptime</li>
                        <li>30 day money back</li>
                    </ul>
                    <a href="#" class="primary-btn">Choose plan</a>
                </div>
            </div>
        </div>
        <div class="row yearly__plans">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="pricing__item">
                    <h4>amerik</h4>
                    <h3>$150 <span>/ month</span></h3>
                    <ul>
                        <li>2,5 GB web space</li>
                        <li>Free site buiding tools</li>
                        <li>Free domain registar</li>
                        <li>24/7 Support</li>
                        <li>Free marketing tool</li>
                        <li>99,9% Services uptime</li>
                        <li>30 day money back</li>
                    </ul>
                    <a href="#" class="primary-btn">Choose plan</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->

<!-- Pricing Section Begin -->
<section  class="pricing-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Fitur Hosting kami</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-television fa-4x text-primary"></i>
                    <h5 class="mt-3">LiteSpeed Teknologi</h5>
                    <p>Membuat kecepatan Akses website anda seperti kilat!</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-object-group fa-4x text-primary"></i>
                    <h5 class="mt-3">Support PHP 7+</h5>
                    <p>Multiple PHP Versions: 4.4 to 7.2

                        LiteSpeed Technologies API for PHP

                        Perl / Tcl / SSI / ROR / CGI / Python

                        GD Library / Image Magick / NetPBM

                    IonCube / OPCache / MemCached</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-map-o fa-4x text-primary"></i>
                    <h5 class="mt-3">SSL Ready</h5>
                    <p>SSL sudah terinstall secara otomatis!</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-diamond fa-4x text-primary"></i>
                    <h5 class="mt-3">Cloudflare Integration</h5>
                    <p>Terdapat Cloudflare pada Dashboard Cpanel</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-television fa-4x text-primary"></i>
                    <h5 class="mt-3">99,99% Uptime!</h5>
                    <p>Kemungkinan Down Sangat minim, membuat website anda selalu online 24jam!</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-object-group fa-4x text-primary"></i>
                    <h5 class="mt-3">Softaculous</h5>
                    <p>Install 1000+ Script dengan mudah dengan softaculous!</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-map-o fa-4x text-primary"></i>
                    <h5 class="mt-3">CronJob Ready</h5>
                    <p>CronJob membuat Task terjadwal secara otomatis!</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <i class="fa fa-diamond fa-4x text-primary"></i>
                    <h5 class="mt-3">DDOS Protection</h5>
                    <p>Aman dari serangan DDOS dan terdapat Virus Scanner, untuk menjamin keamanan server Hosting anda!</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Pricing Section End -->

<!-- Achievement Section Begin -->
<section class="set-bg spad" data-setbg="{{ asset('homepage/img/achievement-bg.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3 class="text-white">HOW TO BUILD YOUR WEBSITE ONLINE TODAY?</h3>
                </div>
                <div class="work__text">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="work__item">
                                <i class="fa fa-desktop"></i>
                                <span>CREATE YOUR OWN WEBSITE WITH OUR</span>
                                <h3>WEB SITE BUILDER</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                                gravida facilisis. </p>
                                <a href="#" class="primary-btn">Read More</a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="work__item">
                                <i class="fa fa-shopping-bag"></i>
                                <span>EASY CREATE, MANAGE & SELL</span>
                                <h3>ONLINE STORE</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                                gravida facilisis. </p>
                                <a href="#" class="primary-btn">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Achievement Section End -->



<!-- Choose Plan Section Begin -->
<section class="choose-plan-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <img src="{{ asset('homepage/img/choose-plan.png')}}" alt="">
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="plan__text">
                    <h3>up to 70% discount with free domain name registration included!</h3>
                    <ul>
                        <li><span class="fa fa-check"></span> FREE Domain Name</li>
                        <li><span class="fa fa-check"></span> FREE Email Address</li>
                        <li><span class="fa fa-check"></span> Plently of disk space</li>
                        <li><span class="fa fa-check"></span> FREE Website Bullder</li>
                        <li><span class="fa fa-check"></span> FREE Marketing Tools</li>
                        <li><span class="fa fa-check"></span> 1-Click WordPress Install</li>
                    </ul>
                    <a href="#" class="primary-btn">Get start now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Choose Plan Section End -->

<!-- Question Section Begin -->
<section class="question-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Have a questions?</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="question__accordin">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-heading active">
                                <a class="active" data-toggle="collapse" data-target="#collapseOne">
                                    How do I cancel and delete my account?
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse
                                        ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel
                                    facilisis. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-heading">
                                <a data-toggle="collapse" data-target="#collapseTwo">
                                    Can I get my website listed in Google?
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse
                                        ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel
                                    facilisis. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-heading">
                                <a data-toggle="collapse" data-target="#collapseThree">
                                    Can I run a business?
                                </a>
                            </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse
                                        ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel
                                    facilisis. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-heading">
                                <a data-toggle="collapse" data-target="#collapseFour">
                                    Does ArkaHost offer phone support?
                                </a>
                            </div>
                            <div id="collapseFour" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse
                                        ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel
                                    facilisis. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <form action="#" class="question-form">
                    <input type="text" placeholder="Name">
                    <input type="text" placeholder="Email">
                    <textarea placeholder="Question"></textarea>
                    <button type="submit" class="site-btn">Send question</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Question Section End -->
@endsection