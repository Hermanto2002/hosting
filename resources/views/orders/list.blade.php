@extends('layouts.dashboard')
@section('page-name','Hostings')


@section('content')


<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title m-b-0">Hosting's Management</h5> <br>
          <a class="btn btn-primary" href="#"><i class="mdi mdi-plus font-16"></i> Add New Hosting</a>
        </div>
        <div class="table-responsive">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">Customer</th>
                <th scope="col">Username</th>
                <th scope="col">Package</th>
                <th scope="col">Order Date</th>
                <th scope="col">Due Date</th>
                <th scope="col">Status</th>
                <th scope="col">#</th>
              </tr>
            </thead>
            <tbody class="customtable">
              @foreach($data['list'] as $item)
              <tr>
                <td><button class="btn btn-sm btn-primary btn-edit-package" data-id="{{ $item->id }}" data-name="{{ $item->title }}" data-toggle="modal" data-target="#editModal">{{ $item->getUser->name }}</button></td>
                <td>{{ $item->username }}</td>
                <td>{{ $item->getProduct->title ?? '' }}</td>
                <td>{{ $item->order_date }}</td>
                <td>{{ $item->due_date }}</td>
                <td>{{ ($item->status == 1)?'Active':'Non-Active/Suspend' }}</td>
                <td>
                  
                  <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Action
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#"> Login Panel</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Suspend</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Delete</a>
                    </div>
                  </div>

                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Package</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('package.update') }}" method="post" id="submitForm">
        @csrf
        <input type="hidden" name="id" value="" id="form-id">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="title" class="form-control" id="form-name">
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-save-package">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('custom-script')
<script>
  $(document).ready(function(){
    $(document).on('click','.btn-edit-package',function(){
      var id = $(this).data('id');
      var name = $(this).data('name');
      $('#form-id').val(id);
      $('#form-name').val(name);
    });
    $(document).on('click','.btn-save-package',function(){
      $('#submitForm').submit();
    });
  });
</script>
@endsection
