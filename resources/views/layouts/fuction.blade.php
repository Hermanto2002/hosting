<?php
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}

function MBtoGB($data)
    {
        if ($data >= 1024)
        {
            $data = number_format($data / 1024, 2) . ' GB';
        }
        elseif ($data > 1)
        {
            $data = $data . ' MB';
        }
        elseif ($data == 1)
        {
            $data = $data . ' MB';
        }
        else
        {
            $data = 'Unlimited';
        }

        return $data;
}
?>