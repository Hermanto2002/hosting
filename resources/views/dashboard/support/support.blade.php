@extends('layouts.dashboard')
@section('page-name','Support')
@section('custom-style')
<style>
  .accordion_two_section {
    /*background: #f7f7f7*/
  }

  .ptb-100 {
    padding-top: 10px;
    padding-bottom: 10px
  }

  .accordionTwo .panel-group {
    margin-bottom: 0
  }

  .accordionTwo .panel {
    background-color: transparent;
    box-shadow: none;
    border-bottom: 10px solid transparent;
    border-radius: 0;
    margin: 0;
    margin-top:10px;
  }

  .accordionTwo .panel-default {
    border: 0
  }

  .accordionTwo .panel-default>.panel-heading {
    background: #0000;
    border-radius: 0px;
    border-color: #0000
  }

  .accordion-wrap .panel-heading {
    padding: 0px;
    border-radius: 0px
  }

  .panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
    color: inherit
  }

  .accordionTwo .panel .panel-heading a.collapsed {
    color: #999999;
    background-color: #fff;
    display: block;
    padding: 12px 20px
  }

  .accordionTwo .panel .panel-heading a {
    display: block;
    padding: 12px 20px;
    /*color: #fff*/
  }

  .accordion-wrap .panel .panel-heading a {
    font-size: 14px
  }

  .accordionTwo .panel-group .panel-heading+.panel-collapse>.panel-body {
    border-top: 0;
    padding-top: 0;
    padding: 20px 20px 20px 30px;
    background: #0000;
    /*color: #fff;*/
    font-size: 14px;
    line-height: 24px
  }

  .accordionTwo .panel .panel-heading a:after {
    content: "\2212";
    color: #4285f4;
    background: #fff
  }

  .accordionTwo .panel .panel-heading a:after,
  .accordionTwo .panel .panel-heading a.collapsed:after {
    font-family: 'FontAwesome';
    font-size: 14px;
    float: right;
    width: 21px;
    display: block;
    height: 21px;
    line-height: 21px;
    text-align: center;
    border-radius: 50%;
    color: #FFF
  }

  .accordionTwo .panel .panel-heading a.collapsed:after {
    content: "\2b";
    color: #fff;
    background-color: #DADADA
  }

  .accordionTwo .panel .panel-heading a:after {
    content: "\2212";
    color: #4285f4;
    background: #dadada
  }

  a:link {
    text-decoration: none
  }
</style>
@endsection
@section('content')
<div class="container-fluid">
  <nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"  aria-selected="true">Knowledgebase</a>
      <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="{{route('dashboard-ticket-list')}}" role="tab"  aria-selected="true">Ticket</a>
    </div>
  </nav>
  <div class="row mt-4">
    <!-- Column -->
    <div class="col-md-4">
      <a href="">
        <div class="card card-hover">
          <div class="box bg-cyan text-center py-4">
            <h1 class="font-light text-white"><i class="fas fa-question"></i></h1>
            <h6 class="text-white">FAQ</h6>
          </div>
        </div>
      </a>
      
    </div>
    <!-- Column -->
    <div class="col-md-4">
      <a href="{{route('dashboard-ticket')}}">
        <div class="card card-hover">
          <div class="box bg-cyan text-center py-4">
            <h1 class="font-light text-white"><i class="fas fa-ticket-alt"></i></i></h1>
            <h6 class="text-white">Open Ticket</h6>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="row mt-4">
    <!-- Column -->
    <div class="col-md-8">
      <div class="card" >
        <section class="accordion_two_section ptb-100" style="box-shadow: 1px 4px 20px 0px #e6e3e3;">
          <div class="container">
            <div class="row">
              <div class="col-12 accordionTwo">
               <h4 class="card-title">Popular Questions</h4>

               <div class="panel-group" id="accordionTwoLeft">
                @foreach ($faq as $item)
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordionTwoLeft" href="#collapse{{$item->id}}" aria-expanded="false" class="collapsed">{{$item->title}}</a> </h4>
                  </div>
                  <div id="collapse{{$item->id}}" class="panel-collapse collapse" aria-expanded="false" role="tablist" style="height: 0px;">
                    <div class="panel-body">{!!$item->content!!}</div>
                  </div>
                </div>
                @endforeach

              </div>
              <a class="btn btn-primary btn-block" href="">View All</a>
              <!--end of /.panel-group-->
            </div>
            <!--end of /.col-sm-6-->
          </div>
        </div>
        <!--end of /.container-->
      </section>
    </div>
  </div>
</div>
</div>
@endsection