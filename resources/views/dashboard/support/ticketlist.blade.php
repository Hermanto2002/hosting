@extends('layouts.dashboard')
@section('page-name','Support')
@section('custom-style')
<style>
  .accordion_two_section {
    /*background: #f7f7f7*/
  }

  .ptb-100 {
    padding-top: 10px;
    padding-bottom: 10px
  }

  .accordionTwo .panel-group {
    margin-bottom: 0
  }

  .accordionTwo .panel {
    background-color: transparent;
    box-shadow: none;
    border-bottom: 10px solid transparent;
    border-radius: 0;
    margin: 0;
    margin-top:10px;
  }

  .accordionTwo .panel-default {
    border: 0
  }

  .accordionTwo .panel-default>.panel-heading {
    background: #0000;
    border-radius: 0px;
    border-color: #0000
  }

  .accordion-wrap .panel-heading {
    padding: 0px;
    border-radius: 0px
  }

  .panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
    color: inherit
  }

  .accordionTwo .panel .panel-heading a.collapsed {
    color: #999999;
    background-color: #fff;
    display: block;
    padding: 12px 20px
  }

  .accordionTwo .panel .panel-heading a {
    display: block;
    padding: 12px 20px;
    /*color: #fff*/
  }

  .accordion-wrap .panel .panel-heading a {
    font-size: 14px
  }

  .accordionTwo .panel-group .panel-heading+.panel-collapse>.panel-body {
    border-top: 0;
    padding-top: 0;
    padding: 20px 20px 20px 30px;
    background: #0000;
    /*color: #fff;*/
    font-size: 14px;
    line-height: 24px
  }

  .accordionTwo .panel .panel-heading a:after {
    content: "\2212";
    color: #4285f4;
    background: #fff
  }

  .accordionTwo .panel .panel-heading a:after,
  .accordionTwo .panel .panel-heading a.collapsed:after {
    font-family: 'FontAwesome';
    font-size: 14px;
    float: right;
    width: 21px;
    display: block;
    height: 21px;
    line-height: 21px;
    text-align: center;
    border-radius: 50%;
    color: #FFF
  }

  .accordionTwo .panel .panel-heading a.collapsed:after {
    content: "\2b";
    color: #fff;
    background-color: #DADADA
  }

  .accordionTwo .panel .panel-heading a:after {
    content: "\2212";
    color: #4285f4;
    background: #dadada
  }

  a:link {
    text-decoration: none
  }
</style>
@endsection
@section('content')
<div class="container-fluid">
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link" href="{{route('dashboard-support')}}">Faq</a>
    </li>
    <li class="nav-item">
      <a class="nav-link active" href="{{route('dashboard-ticket')}}">Ticket</a>
    </li>
  </ul>
  <div class="row mt-4">
    <div class="col-12">
      <div class="card">
        <div class="table-responsive">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col" width="40%">Subject</th>
                <th scope="col">Code</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody class="customtable">
              @foreach ($tickets as $item)
              <tr>
                <td>{{$item->subject}}</td>
                <td>#{{$item->code}}</td>
                <td>{{$item->status}}</td>
                <td>
                  <a class="btn btn-primary" href="">View reply</a>
                </td>
              </tr>
              @endforeach
              
            </tbody>
          </table>
          
        </div>
      </div>

    </div>
  </div>
</div>
@endsection