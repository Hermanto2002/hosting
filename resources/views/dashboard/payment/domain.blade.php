@extends('layouts.dashboard') @section('page-name','Domain Setting') @section('custom-script')
{{-- <script>
  $(function() {
    $("#inputadd").hide();
    $("#btnadd").hide();
    $("#chkDomain").click(function() {
      if ($(this).is(":checked")) {
        $("#inputadd").show();
        $("#inputbuy").hide();

        $("#textadd").show();
        $("#textbuy").hide();

        $("#btnadd").show();
        $("#btnbuy").hide();
      } else {
        $("#inputadd").hide();
        $("#inputbuy").show();

        $("#textadd").hide();
        $("#textbuy").show();

        $("#btnadd").hide();
        $("#btnbuy").show();
      }
    });
  });
</script> --}}
@endsection @section('content')
<div class="container-fluid">
  <div class="row mt-3">
    <div class="col-8">
      <div class="card border mb-3">
        <div class="card-header text-center text-white bg-primary">
          <h5 class="card-title">Cari Nama Domain</h5>
        </div>
        <div class="card-body">
          <div class="accordion" id="accordionExample">
            <div class="card">
              <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Register Domain Baru
                  </button>
                </h2>
              </div>

              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="form-group container py-3">
                  <form action="{{ route('dashboard-domain-add')}}" method="POST">
                    @csrf
                    <input type="hidden" name="statusdomain" value="1">
                    <div class="row">
                      <div class="col-10">
                        <input type="text" name="url" placeholder="contoh.com" value="" class="form-control">
                      </div>
                      <div class="col">
                        <div class="form-group">
                          <select name="domain" class="form-control" id="exampleFormControlSelect1">
                            <option name=".com">.com</option>
                            <option name=".id">.id</option>
                            <option name=".co.id">.co.id</option>
                            <option name=".org">.org</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <button class="btn btn-primary btn-block text-white" type='submit'   id="btnbuy">BELI</button>
                  </form>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Saya sudah punya domain
                  </button>
                </h2>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="form-group container py-3">
                  <form action="{{ route('dashboard-domain-add')}}" method="POST">
                    @csrf
                    <input type="hidden" name="statusdomain" value="0">
                    <div class="row">
                      <div class="col-10">
                        <input type="text" name="url" placeholder="contoh.com" value="" class="form-control">
                      </div>
                      <div class="col">
                        <div class="form-group">
                          <select name="domain" class="form-control" id="exampleFormControlSelect1">
                            <option name=".com">.com</option>
                            <option name=".id">.id</option>
                            <option name=".co.id">.co.id</option>
                            <option name=".org">.org</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <button class="btn btn-primary btn-block text-white" type='submit'   id="btnbuy">NEXT</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
       {{--  <div class="card-body">

          @if($hosting==true)
          <div class="form-group" id="inputadd">
            <form action="{{ route('dashboard-domain-add')}}" method="POST">
              @csrf
              <input type="hidden" name="statusdomain" value="0">
              <label>Masukan nama domain anda</label>
              <div class="row">
                <div class="col-10">
                  <input type="text" name="url" placeholder="contoh.com" value="" class="form-control">
                </div>
                <div class="col">
                  <div class="form-group">
                    <select name="domain" class="form-control" id="exampleFormControlSelect1">
                      <option name=".com">.com</option>
                      <option name=".id">.id</option>
                      <option name=".co.id">.co.id</option>
                      <option name=".org">.org</option>
                    </select>
                  </div>
                </div>
              </div>
            </form>
          </div>
          @endif
          <div class="form-group" id="inputbuy">
            <form action="{{ route('dashboard-domain-add')}}" method="POST">
              @csrf
              <input type="hidden" name="statusdomain" value="1">

              <div class="row">
                <div class="col-8">
                  <input type="text" name="url" placeholder="contoh.com" value="" class="form-control">
                </div>
                <div class="col">
                  <div class="form-group">
                    <select name="domain" class="form-control" id="exampleFormControlSelect1">
                      <option name=".com">.com</option>
                      <option name=".id">.id</option>
                      <option name=".co.id">.co.id</option>
                      <option name=".org">.org</option>
                    </select>
                  </div>
                </div>
              </div>
            </form>
          </div>
          @if($hosting=true)
          <div class="custom-control custom-checkbox my-3">
            <input type="checkbox" class="custom-control-input" id="chkDomain">

            <label class="custom-control-label" for="chkDomain">Sudah punya domain?</label>

          </div> 
          @endif
          <button class="btn btn-primary btn-block text-white"   id="btnbuy">BELI</button>
          <button class="btn btn-primary text-white float-right"  id="btnadd">NEXT</button>
        </div> --}}
      </form>
      
    </div>


  </div>
</div>

</div>
</div>
@endsection