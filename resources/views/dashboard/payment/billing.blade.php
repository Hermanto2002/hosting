@include('layouts.fuction')
@extends('layouts.dashboard')
@section('page-name','Review & Checkout')
@section('custom-style')
<style>
  .btn-close{
    position: relative;
    top: -42px;
    right: -3px;
  }
</style>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row mt-3">
    <div class="col-12 col-md-12 col-lg-8">
      <table class="table table-hover">
        <thead class=" bg-primary">
          <tr class="bg-primary text-white">
            <th scope="col">Product</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>
          @if($session_hosting!=null)
          <tr>
            <th>
              <h3>{{$session_hosting['product']->title}}</h3>
              <h5>{{MBtoGB($session_hosting['product']->quota)}}</h5>
              <h5>{{$session_hosting['domain']}}</h5>
            </th>
            <td>
              <h4>{{rupiah($session_hosting['product']->price)}}
              </h4>
              <h6>Tahunan</h6>
              <a href="{{route('dashboard-cart-host-delete')}}" class="close btn-close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </a>
            </td>
          </tr>
          @endif
          @if($session_domain!=null)
          <tr>
            <th>
              <h3>{{$session_domain['url'].$session_domain['domain']}}</h3>
            </th>
            <td>
              <h4>{{rupiah($session_domain['price'])}}
              </h4>
              <h6>Tahunan</h6>
              <a href="{{route('dashboard-cart-dom-delete')}}" class="close btn-close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </a>
            </td>
          </tr>
          @endif
          @if($session_domain==null && $session_hosting==null)
          <tr>
            <td colspan="2">
              <div class="alert alert-light w-100 text-center" role="alert">
                Tidak ada product dipilih
              </div>
            </td>
          </tr>

          @endif
        </tbody>
      </table>
    </div>
    @if($session_domain!=null || $session_hosting!=null)
    <div class="col-12 col-md-12 col-lg-4">
      <div class="card border">
        <div class="card-header bg-primary text-white">
          Product
        </div>
        <div class="card-body">
          <form action="{{ route('dashboard-payment')}}" method="POST">
            @csrf
            @if($session_hosting!=null)
            <div class="d-flex justify-content-between">
              <div>
                <p>{{$session_hosting['product']->title}}</p>
              </div>
              <div>
                <p>{{rupiah($session_hosting['product']->price)}}</p>
              </div>
            </div>
            @endif
            @if($session_domain!=null)
            <div class="d-flex justify-content-between">
              <div>
                <p>{{$session_domain['url'].$session_domain['domain']}}</p>
              </div>
              <div>
                <p>{{rupiah($session_domain['price'])}}</p>
              </div>
            </div>
            @endif
            <hr>
            <div class="d-flex justify-content-between">
              <div>
                <p>Total</p>
              </div>
              <div>
                <p>{{rupiah($total_price)}}</p>
              </div>
            </div>
            <div class="d-flex justify-content-between">
              <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Coupon Code" aria-label="Recipient's username" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-outline-primary" type="button" id="button-addon2">Reedem</button>
                </div>
              </div>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Payment</button>
          </form>

        </div>
      </div>
    </div>
    @endif
  </div>
</div>
@endsection