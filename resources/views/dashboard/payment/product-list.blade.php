@include('layouts.fuction')
@extends('layouts.dashboard')
@section('page-name','Product')
@section('custom-script')
<script>
  @foreach($Products as $item)
  function price{{$item->id}}() {
    document.getElementById("fname").innerHTML = "{{$item->price}}";
    document.getElementById("fidp").value = "{{$item->id}}";
    document.getElementById("finput").value = "{{$item->price}}";
  }
  @endforeach
</script>
<script>
  $(document).ready(function() {
    $('#example').DataTable( {
      "paging":   true,
      "ordering": true,
      "info":     true
    } );
  } );
</script>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title m-b-0">Your Hosting</h5>
          <a class="btn btn-primary" href="{{url('my/hosting')}}">Add new hosting</a>
        </div>
        <div class="table-responsive">
          <table id="example" class="table " style="width:100%">
            <thead class="thead-light">
              <tr>
                <th scope="col">Product</th>
                <th scope="col">Space</th>
                <th scope="col">Jatuh Tempo</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody class="customtable">
              @foreach ($Orders as $item)
              <tr>
                <td>
                  <b>{{$item->getProduct->title}}</b>
                  {{$item->domain}}
                </td>
                <td>{{MBtoGB($item->getProduct->quota)}}</td>
                <td>
                  @if ($item->status==0)
                  -
                  @elseif($item->status==1)
                  {{$item->due_date}}
                  @endif
                </td>
                <td>
                  @if ($item->status==0)
                  <a class="btn btn-warning" href="{{ route('dashboard-confirm')}}">Payment</a>
                  @elseif($item->status==1)
                  <a class="btn btn-primary" href="">Manage</a>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>

</div>
@endsection