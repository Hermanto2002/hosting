@extends('layouts.dashboard')
@section('page-name','Payment Confirm')
@section('custom-script')
<script type="text/javascript">
  $(window).on('load',function(){
    $('#myModal').modal('show');
  });

  /*datwpicker*/
  jQuery('.mydatepicker').datepicker();
  jQuery('#datepicker-autoclose').datepicker({
    autoclose: true,
    todayHighlight: true,
    dateFormat: 'dd-mm-yy',
  });
  var quill = new Quill('#editor', {
    theme: 'snow'
  });
</script>
@endsection
@section('content')
<div class="modal hide fade" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">How to pay</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Kami menerima pembayaran dari berbagai Bank di Indonesia dengan berbagai cara, diantaranya adalah melalui Internet Banking, Transfer ATM, m-Banking, SMS Banking, Setoran Tunai maupun Phone Banking. Semua pembayaran produk dan layanan dapat dilakukan melalui rekening bank berikut ini:</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <iframe src="https://tawk.to/chat/5e9646c569e9320caac39f89/1e5tglioj" frameborder="0" style="display:block;width:100%;height:80vh;"></iframe>
  
</div>
@endsection