@include('layouts.fuction')
@extends('layouts.dashboard')
@section('page-name','Product')
@section('custom-style')
<style>
  label {
    width: 100%;
  }

  .card-cs-input-element {
    display: none;
  }

  .card-cs-input {
    margin: 10px;
    padding: 00px;
  }

  .card-cs-input:hover {
    cursor: pointer;
  }

  .card{
    border: #c1c1c1 1px solid;
    background: transparent;
  }

  .card-cs-input-element:checked + .card-cs-input {
    background: white;
    box-shadow: 6px 5px 20px 0px #b5b5b5bf;
    border:none;
    /*border: #c1c1c1 1px solid;*/

  }

  /*ribbon card*/
  .box {
    position: relative;
  }

  /* common */
  .ribbon {
    width: 50px;
    height: 50px;
    overflow: hidden;
    position: absolute;
  }
  .card-cs-input-element:checked + .ribbon span{
    display: none !important;
    background-color: #3498db;


  }
  .ribbon::before,
  .ribbon::after {
    position: absolute;
    z-index: -1;
    content: '';
    display: block;
    border: 5px solid #2980b9;
  }
  .ribbon span {
    position: absolute;
    display: block;
    width: 225px;
    padding: 15px 0;
    /*box-shadow: 0 5px 10px rgba(0,0,0,.1);*/
    color: #fff;
    font: 700 18px/1 'Lato', sans-serif;
    text-shadow: 0 1px 1px rgba(0,0,0,.2);
    text-transform: uppercase;
    text-align: center;
  }
  .card-cs-input-element:checked + .ribbon span{
    /*display: none !important;*/
    background-color: #3498db;
    

  }
  /* top right*/
  .ribbon-top-right {
    top: -1px;
    right: -1px;
  }
  .ribbon-top-right::before,
  .ribbon-top-right::after {
    border-top-color: transparent;
    border-right-color: transparent;
  }
  .ribbon-top-right::before {
    top: 0;
    left: 0;
  }
  .ribbon-top-right::after {
    bottom: 0;
    right: 0;
  }
  .ribbon-top-right span {
    left: -25px;
    top: 30px;
    transform: rotate(45deg);
  }
  .footer input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
  }

  .footer input[type=number]::-webkit-inner-spin-button,
  .footer input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }

  .number-input {
    border: 0;
    display: inline-flex;
  }

  .number-input,
  .number-input * {
    box-sizing: border-box;
  }

  .number-input button {
    outline:none;
    -webkit-appearance: none;
    background-color: transparent;
    border: none;
    align-items: center;
    justify-content: center;
    width: 3rem;
    height: 3rem;
    cursor: pointer;
    margin: 0;
    position: relative;
    box-shadow: 0px 0px 1px #474747;
    border-radius: 50%;
    background-position: center;
  }

  .number-input button:before,
  .number-input button:after {
    display: inline-block;
    position: absolute;
    content: '';
    width: 1rem;
    height: 2px;
    background-color: #212121;
    transform: translate(-50%, -50%);
  }
  .number-input button.plus:after {
    transform: translate(-50%, -50%) rotate(90deg);
  }

  .number-input input[type=number] {
    font-family: sans-serif;
    max-width: 5rem;
    padding: .5rem;
    border: none;
    border-width: 0 2px;
    font-size: 2rem;
    height: 3rem;
    font-weight: bold;
    text-align: center;
    color:#9be3df;
  }
  .float{
    position:fixed;
    width:25%;
    bottom:40px;
    right:40px;
    color:#FFF;
    text-align:center;
    transition: width 2s;
    box-shadow: 6px 5px 20px 0px #b5b5b5bf;
  } 
</style>
@endsection
@section('custom-script')
<script>


$(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'produk') {
            $('#box').show();         
       }
       else {
            $('#box').hide();   
       }
   });
});

  @foreach($Products as $item)
  function price{{$item->id}}() {
    document.getElementById("fname").innerHTML = "{{rupiah($item->price)}}";
    document.getElementById("ftitle").innerHTML = "{{$item->title}}";
    document.getElementById("fidp").value = "{{$item->id}}";
    document.getElementById("finput").value = "{{$item->price}}";
  }
  @endforeach


  

</script>

@endsection
@section('content')


<div class="container-fluid">
  <div class="row">

    @foreach($Products as $item)
    <div class="col-md-3">

      <label>
        <input type="radio" name="product" id="produk" class="card-cs-input-element" onclick="if(this.checked){price{{$item->id}}()}" />

        <div class="card card-default card-cs-input py-3 box">
          <div class="card-body"> 
            <h5 class="text-center font-weight-bold">{{ MBtoGB($item->quota)}} SSD</h5>
            <h2 class="text-center font-weight-bold" style="color: #1e348e">{{ rupiah($item->price)}} / Year</h2>
            <hr class="text-center w-25" style="color: #1e348e; border: solid 1px;">
            <h6 class="text-center">Spec</h6>
            <h6 class="text-center"><b>{{ucwords($item->db)}}</b> Database</h6>
            <h6 class="text-center"><b>{{ MBtoGB($item->bandwidth) }}</b> Bandwidth</h6>
          </div>
          <div class="ribbon ribbon-top-right">     
            <span>ribbon</span>
          </div>
        </div>
      </label>

    </div>
    @endforeach
  </div>
  <form action="{{ route('dashboard-hosting-add')}}" method="POST">
    @csrf
    <div class="float card bg-white" id="box" style="display:none">
      <div class="card-body border border-success">
        <h3 id="ftitle" class="product-line-price text-dark">0</h3>
        <h3 id="fname" class="product-line-price text-dark">0</h3>
        <input type="hidden" id="fidp" name="id" value="">
        <button type="submit" class="btn btn-primary btn-lg btn-block mt-2">Deploy Now</button>
      </div>
      
    </div>
  </form>
</div>

@endsection