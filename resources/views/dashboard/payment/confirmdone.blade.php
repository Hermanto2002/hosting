@extends('layouts.dashboard')
@section('page-name','Payment Confirm')
@section('custom-script')
<script type="text/javascript">


  /*datwpicker*/
  jQuery('.mydatepicker').datepicker();
  jQuery('#datepicker-autoclose').datepicker({
    autoclose: true,
    todayHighlight: true,
    dateFormat: 'dd-mm-yy',
  });
  var quill = new Quill('#editor', {
    theme: 'snow'
  });
</script>
@endsection
@section('content')
<div class="modal hide fade" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">How to pay</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit id sequi saepe tenetur deserunt repellendus, soluta temporibus earum eos ipsa quaerat! Quae laboriosam illum placeat obcaecati nostrum deleniti minima esse.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row mt-3">
    <div class="col-8">
      <div class="card">
        <div class="card-body">
          <div class="form-group"> 
            <label>Bank Pengirim</label>
            <input type="text" readonly name="pengirim" value="{{ $transaksi->pengirim }}" class="form-control">
          </div>
          <div class="form-group m-t-20"> 
            <label>Nomor Rekening Pengirim</label>
            <input type="text" readonly name="norek" value="{{ $transaksi->norek }}" class="form-control">
          </div>
          <div class="form-group m-t-20"> 
            <label>Nama Pengirim</label>
            <input type="text" value="{{ $transaksi->nama_pengirim }}" readonly name="nama_pengirim" class="form-control">
          </div>
          <div class="form-group m-t-20"> 
            <label>Tanggal Transfer</label>
            <div class="input-group">
              <input readonly value="{{ $transaksi->tanggal_pembayaran }}" type="date" name="tanggal_pembayaran" class="form-control">
            </div>
          </div>
          <div class="form-group m-t-20">
            <label>Jumlah Transfer</label>
            <input type="text" value="{{ $transaksi->jumlah_transfer }}" name="jumlah_transfer" class="form-control">
          </div>
          <div class="form-group m-t-20">
            <label>Upload Bukti Bayar</label>
            <br>
            <img width="50%" src="{{ asset('images/buktibayar/'.$transaksi->bukti_bayar)}}" alt="">
          </div>
          <div class="form-group m-t-20">
            <label>Catatan</label>
            <textarea readonly class="form-control" name="catatan" rows="3">
              {!! $transaksi->catatan !!}
            </textarea>
          </div>
          <a class="btn btn-primary" href="{{ route('dashboard-confirmedit',['id'=>$transaksi->order_id ])}}">Edit data</a>
        </div>
      </div>
    </div>
    <div class="col-4">
      <div class="card border">
        <div class="card-header">
          Product
        </div>
        <div class="card-body">
          <div class="d-flex justify-content-between">
            <div>
              <p>Nama hosting</p>
            </div>
            <div>
              <p>Harga</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection