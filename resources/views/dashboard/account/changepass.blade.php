@extends('layouts.dashboard')
@section('page-name','')
@section('custon-script')
<script>
  $(".select2").select2();
</script>

@endsection
@section('content')

<div class="container-fluid">
  <div class="card mb-3" style="max-width: 540px;">
    <div class="row no-gutters">
      <div class="col-md-4">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSWBIRL1ON5wYNfIsaa7zXSggRhakTva9i6l3AhkUJfRn_BmZ9L" class="card-img" alt="..." id="img1">

    </div>
    <div class="col-md-8">
        <div class="card-body">
          <span class="display-4" style="font-size: 35px;">{{$user->name}}</span>
          <span class="mt-0">{{$user->email}}</span>
      </div>
  </div>
</div>
</div>

@include('dashboard.account-nav')

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <div class="row mt-3">
        <div class="col-6">
          <h3 class="mt-2">Your Details</h3>
          <form action="{{ route('dashboard-account-updatepss')}}" method="post">
            @csrf
            <div class="form-group mt-3"> 
                <label>Email</label>
                <input type="email" name="email" value="{{$user->email}}" class="form-control">
            </div>
            <div class="form-group mt-3"> 
                <label>New password</label>
                <input type="password" name="password" value="" class="form-control">
            </div>
            <input type="hidden" name="id" value="{{$user->id}}">
            <input type="submit" value="update" class="btn btn-primary">
        </form>

    </div>
    <div class="col-md-3">


    </div>
</div>
</div>
</div>
</div>
<script>
  function file_changed(){
    var selectedFile = document.getElementById('input').files[0];
    var img1 = document.getElementById('img1')
    var img2 = document.getElementById('img2')

    var reader = new FileReader();
    reader.onload = function(){
       img1.src = this.result
       img2.src = this.result
   }
   reader.readAsDataURL(selectedFile);
}
</script>
@endsection