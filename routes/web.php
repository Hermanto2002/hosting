<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


// halaman domain checker
Route::get('/domain_check/{domain}','Module\DomainController@check')->name('check-domain');


//homepage
Route::get('/', 'homepage\IndexController@index')->name('homepage-index');
Route::get('/about', 'homepage\IndexController@about')->name('homepage-about');
// Route::get('/about', 'homepage\IndexController@about')->name('homepage-about');


//halaman yang hanya bisa diakses oleh role customer
Route::middleware('customer')->prefix('my')->group(function(){
	Route::get('/', 'dashboard\IndexController@index')->name('dashboard-index');
	Route::get('/hosting', 'dashboard\IndexController@add')->name('dashboard-add');

	//payment
	Route::get('/hosting', 'dashboard\PaymentController@hosting')->name('dashboard-hosting');
	Route::post('/hosting/add', 'dashboard\PaymentController@hostingadd')->name('dashboard-hosting-add');

	Route::get('/domain', 'dashboard\PaymentController@domain')->name('dashboard-domain');
	Route::post('/domain/add', 'dashboard\PaymentController@domainadd')->name('dashboard-domain-add');


	// Route::post('/billing', 'dashboard\PaymentController@billing')->name('dashboard-billing');
	Route::get('/checkout', 'dashboard\PaymentController@checkout')->name('dashboard-checkout');
	Route::post('/payment', 'dashboard\PaymentController@payment')->name('dashboard-payment');
	Route::get('/confirm', 'dashboard\PaymentController@confirm')->name('dashboard-confirm');

	//user
	

	//Support
	Route::get('/support', 'dashboard\SupportController@support')->name('dashboard-support');
	Route::get('/support/ticket', 'dashboard\SupportController@ticket')->name('dashboard-ticket');
	Route::post('/support/ticket/add', 'dashboard\SupportController@ticketadd')->name('dashboard-ticket-add');
	Route::get('/support/ticket/list', 'dashboard\SupportController@ticketlist')->name('dashboard-ticket-list');
	Route::post('/support/ticket/reply', 'dashboard\SupportController@ticketreply')->name('dashboard-ticket-reply');

	//cart-delete
	Route::get('/cart/delete/hosting', 'dashboard\PaymentController@carthostdelete')->name('dashboard-cart-host-delete');
	Route::get('/cart/delete/domain', 'dashboard\PaymentController@cartdomdelete')->name('dashboard-cart-dom-delete');

});

Route::middleware('admin')->prefix('admin')->group(function(){
	Route::get('/', function () {
		echo "ini halaman admin";
	})->name('admin-index');
	// hosting automation

	Route::get('/hosting-create', 'Module\HostingController@create')->name('module.hosting.create');
	Route::post('/hosting-build', 'Module\HostingController@build')->name('module.hosting.build');
	Route::get('/hosting-packages', 'Module\HostingController@packages')->name('module.hosting.packages');
	Route::get('/hosting-package/{name}', 'Module\HostingController@package')->name('module.hosting.package');
	// Route::get('/hosting-delete/{name}', 'Module\HostingController@delete')->name('module.hosting.delete'); bahaya, jangan di enable!

	Route::get('/orders', 'OrderController@index')->name('order.list');
	// Route::get('/order-add', 'OrderController@create')->name('order.create');
	Route::post('/order-store', 'OrderController@store')->name('order.store');
	// Route::get('/order-edit', 'OrderController@edit')->name('order.edit');
	Route::post('/order-update', 'OrderController@update')->name('order.update');
	Route::get('/order-delete/{id}', 'OrderController@destroy')->name('order.destroy');

	Route::get('/customers', 'CustomerController@index')->name('customer.list');
	// Route::get('/customer-add', 'CustomerController@create')->name('customer.create');
	Route::post('/customer-store', 'CustomerController@store')->name('customer.store');
	// Route::get('/customer-edit', 'CustomerController@edit')->name('customer.edit');
	Route::post('/customer-update', 'CustomerController@update')->name('customer.update');
	Route::get('/customer-delete/{id}', 'CustomerController@destroy')->name('customer.destroy');


	Route::get('/packages', 'ProductController@index')->name('package.list');
	// Route::get('/package-add', 'ProductController@create')->name('package.create');
	Route::post('/package-store', 'ProductController@store')->name('package.store');
	// Route::get('/package-edit', 'ProductController@edit')->name('package.edit');
	Route::post('/package-update', 'ProductController@update')->name('package.update');
	Route::get('/package-delete/{id}', 'ProductController@destroy')->name('package.destroy');
	Route::get('/package-syncronize', 'ProductController@sync')->name('package.sync');

	
	

}); 

Route::middleware('auth')->group(function(){
	Route::get('/account', 'dashboard\AccountController@account')->name('dashboard-account');
	Route::post('/account/update', 'dashboard\AccountController@update')->name('dashboard-account-update');
	Route::get('/account/changepass', 'dashboard\AccountController@changepss')->name('dashboard-account-changepss');
	Route::post('/account/changepass/update', 'dashboard\AccountController@updatepss')->name('dashboard-account-updatepss');
});