-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Apr 2020 pada 08.10
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_hosting`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq`
--

CREATE TABLE `faq` (
  `id` int(10) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `category` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `faq`
--

INSERT INTO `faq` (`id`, `title`, `content`, `category`, `created_at`, `updated_at`) VALUES
(1, 'test', 'akhcoasajdhskd\r\nloermakhsdoajdkajsgka,fb c', 1, '2020-03-31 23:15:03', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq-category`
--

CREATE TABLE `faq-category` (
  `id` int(10) NOT NULL,
  `category` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_17_085854_create_permission_tables', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(100) NOT NULL,
  `product_id` int(10) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `due_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `product_id`, `user_id`, `username`, `password`, `price`, `order_date`, `due_date`, `status`, `created_at`, `updated_at`) VALUES
(10, 6, '4', NULL, NULL, '1250', '2020-03-31 15:25:41', '2020-03-31 15:25:41', '0', '2020-03-31 07:25:41', '2020-03-31 07:25:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `cpu` int(10) DEFAULT NULL,
  `quota` varchar(100) DEFAULT NULL,
  `bandwidth` varchar(100) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `db` varchar(100) DEFAULT NULL,
  `ftp` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `domain` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `created_at`, `updated_at`, `cpu`, `quota`, `bandwidth`, `email`, `db`, `ftp`, `code`, `domain`) VALUES
(4, 'BIG', '5000', '2020-03-30', '2020-03-30', NULL, '1024', '102400', 'unlimited', 'unlimited', 'unlimited', 'BIG', '3'),
(5, 'STARTER', '2500', '2020-03-30', '2020-03-30', NULL, '150', '15360', '2', '1', '1', 'STARTER', '1'),
(6, 'SUPER', '1250', '2020-03-30', '2020-03-30', NULL, '2048', '204800', 'unlimited', 'unlimited', 'unlimited', 'SUPER', '3'),
(7, 'Small', '1000', '2020-03-30', '2020-03-30', NULL, '250', '20480', '10', '5', '10', 'Small', '1'),
(8, 'Standard', '500', '2020-03-30', '2020-03-30', NULL, '600', '56320', 'unlimited', 'unlimited', 'unlimited', 'Standard', '3'),
(9, 'Unlimited', '10000', '2020-03-30', '2020-03-30', NULL, 'unlimited', 'unlimited', 'unlimited', 'unlimited', 'unlimited', 'Unlimited', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket`
--

CREATE TABLE `ticket` (
  `id` int(10) NOT NULL,
  `id_user` int(10) DEFAULT NULL,
  `id_admin` int(10) DEFAULT NULL,
  `id_product` int(10) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `message` text,
  `file` varchar(100) DEFAULT NULL,
  `response` text,
  `code` varchar(10) DEFAULT NULL,
  `category` varchar(10) DEFAULT NULL,
  `status` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ticket`
--

INSERT INTO `ticket` (`id`, `id_user`, `id_admin`, `id_product`, `subject`, `message`, `file`, `response`, `code`, `category`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, NULL, 2, NULL, 'qwqwqwqwqwqwq', NULL, NULL, '468383', '2', 0, '2020-03-31 15:01:19', '2020-03-31 15:01:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(100) NOT NULL,
  `order_id` int(10) DEFAULT NULL,
  `tanggal_pembayaran` date DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `prihal` text,
  `bukti_bayar` varchar(100) DEFAULT NULL,
  `nama_pengirim` varchar(100) DEFAULT NULL,
  `tanggal_transfer` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jumlah_transfer` int(100) DEFAULT NULL,
  `catatan` text,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `order_id`, `tanggal_pembayaran`, `user_id`, `prihal`, `bukti_bayar`, `nama_pengirim`, `tanggal_transfer`, `jumlah_transfer`, `catatan`, `status`, `created_at`, `updated_at`) VALUES
('tf-33390', 1, '2020-03-29', 3, NULL, '1585324587.jpg', 'ucok', '2020-03-28 18:46:15', 23232, 'wafegrrg\r\n              edit', 'Belum Dibayar', '2020-03-27 07:56:27', '2020-03-28 10:46:15'),
('tf-61049', 5, '2020-03-24', 3, NULL, NULL, 'Bosqu', '2020-03-28 19:03:41', 100000, 'wsdscwswsd', '0', '2020-03-28 11:03:41', '2020-03-28 11:03:41'),
('tf-98383', 6, '2020-03-27', 3, NULL, NULL, 'Bosqu', '2020-03-28 19:04:53', 100000, NULL, '0', '2020-03-28 11:04:53', '2020-03-28 11:04:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weburl` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `phone`, `address`, `city`, `zipcode`, `country`, `company`, `weburl`, `image`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'admin', 'admin@admin.com', 'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$rKF.6dXnH3.ty.9I3pE4xeMGTnjKR9MJX9hgimfo7sBfE5TrQYYi2', NULL, '2020-03-15 04:11:02', '2020-03-15 04:11:02'),
(4, 'I Made Hermanto', 'cust@cust.com', 'Customer', '0895630687322', 'Jalan Pasekan, Banjar Batuaji, Batubulan Kangin, Sukawati', 'Gianyar', '80582', 'Indonesia', 'Web Programmer', 'Web Programmer', NULL, NULL, '$2y$10$/W8IxcU6a3FB3ByqqiA89e.r8ltDEvaLyCVKbEH..ju4952bHNXXi', NULL, '2020-03-30 04:01:03', '2020-03-31 22:08:43');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `faq-category`
--
ALTER TABLE `faq-category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `faq-category`
--
ALTER TABLE `faq-category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
