<?php 

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

return [
    'US'=>[
        'HOSTING_HOST' => 'raine.serverko.net',
        'HOSTING_PORT' => '2222',
        'HOSTING_IP' => '45.35.56.26',
        'HOSTING_NOTIF' => 'yes',
        'HOSTING_ADM_USR' => 'hostingburn',
        'HOSTING_ADM_PW' => '8Q~Gaz].NPsF',
        'HOSTING_PW' => generateRandomString(),
    ],
    'ID'=>[
        'HOSTING_HOST' => 'cloud.gotrasoft.com',
        'HOSTING_PORT' => '2222',
        'HOSTING_IP' => '103.133.223.138',
        'HOSTING_NOTIF' => 'yes',
        'HOSTING_ADM_USR' => 'cloudgot',
        'HOSTING_ADM_PW' => 'L21tl92fpZ',
        'HOSTING_PW' => generateRandomString(),
    ],


];