<?php

namespace App\Http\Controllers\homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class IndexController extends Controller
{
    public function index()
    {
    	$product=Product::all();
    	return view('homepage.index',compact('product'));
    }

    public function about()
    {
    	$product=Product::all();
    	return view('homepage.about',compact('product'));
    }

}
