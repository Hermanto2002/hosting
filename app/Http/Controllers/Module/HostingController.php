<?php

namespace App\Http\Controllers\Module;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Module\HTTPSocket as HTTPSocket;

class HostingController extends Controller
{
    protected $host;
    protected $port;
    protected $adm_usr;
    protected $adm_pw;
    protected $ip;
    protected $pw;
    protected $notif;

    function __construct()
    {
    	 $this->host = \Config::get('global.US.HOSTING_HOST');
    	 $this->port = \Config::get('global.US.HOSTING_PORT');
    	 $this->adm_usr = \Config::get('global.US.HOSTING_ADM_USR');
    	 $this->adm_pw = \Config::get('global.US.HOSTING_ADM_PW');
	    $this->ip = \Config::get('global.US.HOSTING_IP');
	    $this->pw = \Config::get('global.US.HOSTING_PW');
	    $this->notif = \Config::get('global.US.HOSTING_NOTIF');
    }

    public function create()
    {
    	// Load Package Hosting

    	return view('module.hosting.create');
    }


    public function build(Request $request)
    {

    	if ($request->server == 'ID') {
    		$this->host = \Config::get('global.ID.HOSTING_HOST');
	    	 $this->port = \Config::get('global.ID.HOSTING_PORT');
	    	 $this->adm_usr = \Config::get('global.ID.HOSTING_ADM_USR');
	    	 $this->adm_pw = \Config::get('global.ID.HOSTING_ADM_PW');
		    $this->ip = \Config::get('global.ID.HOSTING_IP');
		    $this->pw = \Config::get('global.ID.HOSTING_PW');
		    $this->notif = \Config::get('global.ID.HOSTING_NOTIF');
    	}

    	$sock = new HTTPSocket;

    	$sock->connect('ssl://'.$this->host,$this->port);

		$sock->set_login($this->adm_usr,$this->adm_pw);

		$sock->set_method('POST');

		$sock->query('/CMD_API_ACCOUNT_USER',
		array(
			'action' => 'create',
			'add' => 'Submit',
			'username' => explode('.', $request->domain)[0],
			'email' => $request->email,
			'passwd' => $this->pw,
			'passwd2' => $this->pw,
			'domain' => $request->domain,
			'package' => $request->package,
			'ip' => $this->ip,
			'notify' => $this->notif
		));
 
		$result = $sock->fetch_parsed_body();
	 
		if ($result['error'] != "0")
		{
			echo "<b>Error Creating account ".explode('.', $request->domain)[0]." on server<br>\n";
			echo $result['text']."<br>\n";
			echo $result['details']."<br></b>\n";
		}
		else
		{
			echo explode('.', $request->domain)[0]." created on server<br>\n";
		}

		exit(0);

    }


    public function packages(Request $request)
    {
    	if ($_GET['server'] == 'ID') {
    		$this->host = \Config::get('global.ID.HOSTING_HOST');
	    	 $this->port = \Config::get('global.ID.HOSTING_PORT');
	    	 $this->adm_usr = \Config::get('global.ID.HOSTING_ADM_USR');
	    	 $this->adm_pw = \Config::get('global.ID.HOSTING_ADM_PW');
		    $this->ip = \Config::get('global.ID.HOSTING_IP');
		    $this->pw = \Config::get('global.ID.HOSTING_PW');
		    $this->notif = \Config::get('global.ID.HOSTING_NOTIF');
    	}

    	$sock = new HTTPSocket;

    	$sock->connect('ssl://'.$this->host,$this->port);

		$sock->set_login($this->adm_usr,$this->adm_pw);

		$sock->set_method('GET');

		$sock->query('/CMD_API_PACKAGES_USER');
 
		$result = $sock->fetch_parsed_body();
	 
		if (isset($result['error']) && $result['error'] != "0")
		{
			echo $result['text']."<br>\n";
			echo $result['details']."<br></b>\n";
		}
		else
		{
			foreach ($result['list'] as $item) {
				echo $item .'<br/>';
			}
		}

		exit(0);
    }

    public function package($name='',Request $request)
    {
    	if ($_GET['server'] == 'ID') {
    		$this->host = \Config::get('global.ID.HOSTING_HOST');
	    	 $this->port = \Config::get('global.ID.HOSTING_PORT');
	    	 $this->adm_usr = \Config::get('global.ID.HOSTING_ADM_USR');
	    	 $this->adm_pw = \Config::get('global.ID.HOSTING_ADM_PW');
		    $this->ip = \Config::get('global.ID.HOSTING_IP');
		    $this->pw = \Config::get('global.ID.HOSTING_PW');
		    $this->notif = \Config::get('global.ID.HOSTING_NOTIF');
    	}

    	$sock = new HTTPSocket;

    	$sock->connect('ssl://'.$this->host,$this->port);

		$sock->set_login($this->adm_usr,$this->adm_pw);

		$sock->set_method('GET');

		$sock->query('/CMD_API_PACKAGES_USER',array('package'=>$name));
 
		$result = $sock->fetch_parsed_body();
	 
		if (isset($result['error']) && $result['error'] != "0")
		{
			echo $result['text']."<br>\n";
			echo $result['details']."<br></b>\n";
		}
		else
		{
			foreach ($result as $key => $value) {
				echo $key.' : '.$value.'<br/>';
			}
		}

		exit(0);
    }

    public function delete($name='',Request $request)
    {

    	if ($_GET['server'] == 'ID') {
    		$this->host = \Config::get('global.ID.HOSTING_HOST');
	    	 $this->port = \Config::get('global.ID.HOSTING_PORT');
	    	 $this->adm_usr = \Config::get('global.ID.HOSTING_ADM_USR');
	    	 $this->adm_pw = \Config::get('global.ID.HOSTING_ADM_PW');
		    $this->ip = \Config::get('global.ID.HOSTING_IP');
		    $this->pw = \Config::get('global.ID.HOSTING_PW');
		    $this->notif = \Config::get('global.ID.HOSTING_NOTIF');
    	}

    	$sock = new HTTPSocket;

    	$sock->connect('ssl://'.$this->host,$this->port);

		$sock->set_login($this->adm_usr,$this->adm_pw);

		$sock->set_method('POST');

		$sock->query('/CMD_API_SELECT_USERS',array('select0'=>$name,'confirmed'=>'Confirm','delete'=>'yes'));
 
		$result = $sock->fetch_parsed_body();
	 
		if (isset($result['error']) && $result['error'] != "0")
		{
			echo $result['text']."<br>\n";
			echo $result['details']."<br></b>\n";
		}
		else
		{
			foreach ($result as $key => $value) {
				echo $key.' : '.$value.'<br/>';
			}
		}

		exit(0);
    }



}
