<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\transaksi;
use Auth;
use Session;


class PaymentController extends Controller
{
	//hosting
	public function hosting()
	{
		$Products=Product::where('price','<>','')->orderBy('price','ASC')->get();
		return view('dashboard.payment.product', compact('Products'));
	}
	public function hostingadd(Request $request)
	{
		$data = array(
			'id_hosting' => request('id'),
		);
		$product=Product::where('id',$data['id_hosting'])->first();
		$price=$product->price;
		$data['price']=$price;
		$data['product']=$product;
		$cart=Session::get('hosting');
		$cart = array(
			"product" => $data['product'],
		);
		Session::put('hosting', $cart);
		return redirect()->route('dashboard-domain');
	}


	//domain
	public function domain()
	{
		$hosting=Session::get('hosting');
		return view('dashboard.payment.domain', compact('hosting'));
	}
	public function domainadd(Request $request)
	{
		$data = array(
			'url' => request('url'),
			'domain' => request('domain'),
			'statusdomain' => request('statusdomain'),
		);
		// dd($data);
		if ($data['statusdomain']=='1') {
			$cart=Session::get('domain');
			$cart = array(
				"url" => $data['url'],
				"domain" => $data['domain'],
				"price" => "150000",
			);
			Session::put('domain', $cart);
		}
		$domain=$data['url'].$data['domain'];
		Session::put('hosting.domain', $domain);
		// dd(Session::get('hosting'));
		return redirect()->route('dashboard-checkout');
	}

	public function checkout(Request $request)
	{

		$session_domain=Session::get('domain');	
		$session_hosting=Session::get('hosting');
		// dd();

		if ($session_hosting==null) {
			$harga_hosting='0';
		}
		else{
			$harga_hosting=$session_hosting['product']->price;
			// dd($harga_hosting);
		}

		if ($session_domain==null) {
			$harga_domain='0';
		}
		else{
			$harga_domain=$session_domain['price'];
		}
		$total_price=$harga_hosting+$harga_domain;
		$cart=Session::get('billing');
		$cart = array(
			"total" => $total_price,
		);
		Session::put('billing', $cart);
		return view('dashboard.payment.billing', compact('session_domain','session_hosting','total_price'));

	}

	public function confirm()
	{
		$session_domain=Session::get('domain');
		$session_hosting=Session::get('hosting');
		$session_billing=Session::get('billing');

		
		return view('dashboard.payment.confirm');
	}
	public function confirmedit($id)
	{
		$order_id=$id;
		$transaksi=transaksi::where('order_id',$order_id)->first();

		$form_mode="edit";
		return view('dashboard.payment.confirm',compact('transaksi','form_mode','order_id'));
	}

	public function confirmadd(request $request)
	{
		$number = 'tf-'.rand(00001, 99999);
		$status= 0;
		$user_id = Auth::id();
		$cek = transaksi::where('id_transaksi',$number)->exists();
		if ($cek == true) {
			return redirect()->route('payment-confirm-add');
		}
		$data=[
			'id_transaksi'=>$number,
			'order_id'=> request('order_id'),
			'tanggal_pembayaran'=> request('tanggal_pembayaran'),
			'nama_pengirim'=> request('nama_pengirim'),
			'jumlah_transfer'=> request('jumlah_transfer'),
			'catatan'=> request('catatan'),
			'user_id'=> $user_id,
			'status'=> $status,
		];
		if ($request->hasFile('buktibayar')) {
			$file = $request->file('buktibayar');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/buktibayar/', $filename);
            $data['bukti_bayar'] = $filename;
        }
        transaksi::create($data);
        return redirect()->route('dashboard-index');
    }

    public function confirmupdate(request $request)
    {
    	$update = transaksi::where('id_transaksi',$request->id);

    	$data=[
    		'tanggal_pembayaran'=> request('tanggal_pembayaran'),
    		'nama_pengirim'=> request('nama_pengirim'),
    		'jumlah_transfer'=> request('jumlah_transfer'),
    		'catatan'=> request('catatan'),
    	];
    	if ($request->hasFile('buktibayar')) {
    		$file = $request->file('buktibayar');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/buktibayar/', $filename);
            $data['bukti_bayar'] = $filename;
        }
        $update->update($data);
        return redirect()->route('dashboard-index');
    }

    public function carthostdelete()
    {
    	session()->forget('hosting');
    	return redirect()->back();
    }

    public function cartdomdelete()
    {
    	session()->forget('domain');
    	return redirect()->back();
    }

    public function payment()
    {
    	$session_domain=Session::get('domain');
    	$session_hosting=Session::get('hosting');

    	$id_transaksi=rand(00000,99999);
    	$cek=Order::where('id',$id_transaksi)->exists();
    	if ($cek==true) {
    		$id_transaksi=rand(00000,99999);
    	}
    	$data = array(
    		'id' => $id_transaksi, 
    		'product_id'=>$session_hosting['product']->id,
    		'price'=>$session_hosting['product']->price,
    		'user_id'=>Auth::user()->id,
    		'status'=>'0',
    	);
    	Order::create($data);
    	return redirect()->route('dashboard-confirm');

    }
}

