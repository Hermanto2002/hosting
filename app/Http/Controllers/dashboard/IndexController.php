<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use Auth;

class IndexController extends Controller
{

	public function index()
	{
		$user_id=Auth::user()->id;
		$Products=Product::where('price','<>','')->orderBy('price','ASC')->get();
		$Orders_check=Order::where('user_id',$user_id)->first();
		$Orders=Order::where('user_id',$user_id)->get();
		if ($Orders_check!=null) {
			return view('dashboard.payment.product-list', compact('Orders','Products'));
		}
		else{
			return view('dashboard.payment.product', compact('Products'));
		}

	}

	public function add()
	{
		$Products=Product::where('price','<>','')->orderBy('price','ASC')->get();
		return view('dashboard.payment.product', compact('Products'));
	}
}
