<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ticket;
use App\Faq;
use App\Order;
use Auth;

class SupportController extends Controller
{
    public function support()
    {
    	$faq=Faq::where('category',1)->get();
    	return view('dashboard.support.support', compact('faq'));
    }

    public function ticket()
    {
    	$user_id=Auth::user()->id;
    	$order=Order::where('user_id',$user_id)->get();
    	return view('dashboard.support.ticket',compact('order'));
    }

    public function ticketadd(Request $request)
    {
    	$user_id=Auth::user()->id;
    	$code=$user_id.rand(00001, 99999);
    	$cek = Ticket::where('code',$code)->exists();
		if ($cek == true) {
			return redirect()->route('dashboard-ticket-add');
		}
    	$data=[
    		'id_user'=>$user_id,
    		'id_product'=>request('id_product'),
    		'subject'=>request('subject'),
    		'message'=>request('message'),
    		'code'=>$code,
    		'category'=>request('category'),
    		'status'=>0,
    	];
    	Ticket::create($data);
    	return redirect()->route('dashboard-ticket-list');
    }

    public function ticketlist()
    {
    	$user_id=Auth::user()->id;
    	$tickets=Ticket::where('id_user',$user_id)->get();
    	return view('dashboard.support.ticketlist',compact('tickets'));
    }

}
