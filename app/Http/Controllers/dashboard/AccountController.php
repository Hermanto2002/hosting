<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class AccountController extends Controller
{
    public function account()
    {
    	$user_id=Auth::user()->id;
    	$user=user::where('id',$user_id)->first();
    	return view('dashboard.account.account',compact('user'));
    }

    public function update(request $request)
    {
    	$update = User::where('id',$request->id)->first();
		$data=[
			'name'=> request('name'),
			'email'=> request('email'),
			'phone'=> request('phone'),
			'address'=> request('address'),
			'city'=> request('city'),
			'zipcode'=> request('zipcode'),
			'country'=> request('country'),
			'company'=> request('company'),
			'weburl'=> request('weburl'),
		];
		if ($request->hasFile('image')) {

            if ($update->image != "") {
                @unlink('images/user/'.$update->image);
            }

            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/user/', $filename);
            $data['image'] = $filename;
        }
        $update->update($data);
        return redirect()->route('dashboard-account');
    }

    public function updateimg(request $request)
    {
    	$update = User::find($request->id);
    	$data=[
    		'name'=> request('name'),
    		'image'=> request('image'),
    	];
    	dd($data);
    	if ($request->hasFile('image')) {

            if ($update->image != "") {
                @unlink('images/user/'.$update->image);
            }

            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/user/', $filename);
            $data['image'] = $filename;
        }
        $update->update($data);
        // dd($update);
        return redirect()->route('dashboard-account');
    }

    public function changepss()
    {
        $user_id=Auth::user()->id;
        $user=user::where('id',$user_id)->first();
        return view('dashboard.account.changepass',compact('user'));
    }

    public function updatepss(request $request)
    {
        $user_email=Auth::user()->email;
        $email=request('email');
        if ($user_email!=$email) {
            return redirect()->back();
        }
        $password=Hash::make(request('password'));
        // dd($password);
        $user=User::find($request->id);
        $data=[
            'password'=>$password,
        ];
        $user->update($data);
        return redirect()->route('dashboard-account');
    }
}
