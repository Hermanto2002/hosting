<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Module\HTTPSocket as HTTPSocket;

class ProductController extends Controller
{
    protected $host;
    protected $port;
    protected $adm_usr;
    protected $adm_pw;
    protected $ip;
    protected $pw;
    protected $notif;

    function __construct()
    {
        $this->host = \Config::get('global.HOSTING_HOST');
        $this->port = \Config::get('global.HOSTING_PORT');
        $this->adm_usr = \Config::get('global.HOSTING_ADM_USR');
        $this->adm_pw = \Config::get('global.HOSTING_ADM_PW');
        $this->ip = \Config::get('global.HOSTING_IP');
        $this->pw = \Config::get('global.HOSTING_PW');
        $this->notif = \Config::get('global.HOSTING_NOTIF');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list'] = Product::get();
        return view('packages.list', compact('data'));
    }

    public function sync()
    {
        $sock = new HTTPSocket;

        $sock->connect('ssl://'.$this->host,$this->port);

        $sock->set_login($this->adm_usr,$this->adm_pw);

        $sock->set_method('GET');

        $sock->query('/CMD_API_PACKAGES_USER');
 
        $result = $sock->fetch_parsed_body();
     
        if (isset($result['error']) && $result['error'] != "0")
        {
            echo $result['text']."<br>\n";
            echo $result['details']."<br></b>\n";
        }
        else
        {
            foreach ($result['list'] as $item) {
                $detail = $this->package_detail($item);
                // detect old data
                $old = Product::where('code',$item);
                $old_jum = $old->count();
                $old_data = $old->first();
                if($old_jum > 0){
                    $old_data->quota = $detail['quota'];
                    $old_data->bandwidth = $detail['bandwidth'];
                    $old_data->ftp = $detail['ftp'];
                    $old_data->email = $detail['nemails'];
                    $old_data->db = $detail['mysql'];
                    $old_data->domain = $detail['vdomains'];

                    $old_data->update();
                }else{
                    $store = new Product;
                    $store->title = $item;
                    $store->code = $item;
                    $store->quota = $detail['quota'];
                    $store->bandwidth = $detail['bandwidth'];
                    $store->ftp = $detail['ftp'];
                    $store->email = $detail['nemails'];
                    $store->db = $detail['mysql'];
                    $store->domain = $detail['vdomains'];

                    $store->save();

                }
            }
        }
        return redirect()->back();
    }

    public function package_detail($name='')
    {
        $sock = new HTTPSocket;

        $sock->connect('ssl://'.$this->host,$this->port);

        $sock->set_login($this->adm_usr,$this->adm_pw);

        $sock->set_method('GET');

        $sock->query('/CMD_API_PACKAGES_USER',array('package'=>$name));
 
        $result = $sock->fetch_parsed_body();
     
        if (isset($result['error']) && $result['error'] != "0")
        {
            echo $result['text']."<br>\n";
            echo $result['details']."<br></b>\n";
        }
        else
        {
            return $result;
        }

        exit(0);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $new = Product::find($request->id);
        $new->title = $request->title;
        $new->price = $request->price;
        $new->update();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
