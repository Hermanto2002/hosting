<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $guarded = [];
    
    public function getProduct()
    {
    	return $this->belongsTo('App\Product','product_id');
    }
    public function getUser()
    {
    	return $this->belongsTo('App\User','user_id');
    }
}
